export const allServices = {
  addStudent,
  viewStudents
};

//mengatur student localstorage dari "data student" yang dilempar
//jika di data student ada isinya maka lakukan:
//dapatkan "data student" dari localstorage dengan key "students" lalu dimasukan ke variabel students
//"data students" dimasukkan ke students variable
//jika student tidak ada data yg diisikan maka kembalian berupa null
function addStudent(student) {
  if (student) {
    var students = JSON.parse(localStorage.getItem('students'));
    if (students === null) {
      students = [];
    }
    students.push(student);
    localStorage.setItem('students', JSON.stringify(students));
    return student;
  }
  return null;
}
//mengecek data yang sudah di set di localstorage
//jika tidak ada, maka di set array kosong
function viewStudents() {
  var students = JSON.parse(localStorage.getItem('students'));
  return students === null ? [] : students;
}