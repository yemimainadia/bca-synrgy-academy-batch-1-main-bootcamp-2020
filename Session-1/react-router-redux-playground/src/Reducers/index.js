import { crudStudent } from './CrudStudent.reducer.js';
import { viewStudents } from './ViewStudents.reducer.js';
import { auth } from './Login.reducer.js';
import { combineReducers } from 'redux';

//mengkombinasikan / merge -> auth, crudStudent, sama viewStudents
//sehingga dapat diakses dengan 1 variable yakni rooReducer
const rootReducer = combineReducers({
  auth,
  crudStudent,
  viewStudents
});

export default rootReducer;