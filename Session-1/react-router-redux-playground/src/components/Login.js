import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuBar from '../components/MenuBar';
import { allActions } from '../Actions/AllActions';
import { connect } from 'react-redux';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  input: {
    margin: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  gapSmall: {
    marginTop: 50,
  }
});

class Login extends React.Component {

  constructor(props) {
    super(props); ////supaya bisa akses props dari component lain atau parentnya
    this.state = {  //set state supaya bisa dipakai berulang kali
      email: '',
      password: ''
    };
  }

  handleChange(e) { //ketika ada perubahan dari value maka akan melakukan set state
    this.setState({ ...this.state, [e.target.name]: e.target.value })
  }

  handleSubmit(e) { //ketika ada submit, maka akan mengirim hasil inputan sesuai dengan variabel yg sudah ditentukan melalui dispatch
    e.preventDefault(); //posting data via server diblok
    console.log(this.state)
    console.log("this props")
    console.log(this.props.dispatch)
    const { dispatch } = this.props;
    dispatch(allActions.loginUser(this.state));
    this.setState({
      email: '',
      password: ''
    });
  }

  render() {
    const { classes } = this.props; //mengambil value dari sebuah object atau array
    console.log(classes);
    return (
      <div>
        <MenuBar />
        <h1>Login</h1>
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <TextField name="email" value={this.state.firstName} label="Email" className={classes.input} inputProps={{ 'aria-label': 'Description', }} onChange={(e) => this.handleChange(e)} /><br />
          <TextField name="password" value={this.state.lastName} label="Password" className={classes.input} inputProps={{ 'aria-label': 'Description', }} type="password" onChange={(e) => this.handleChange(e)} />
          <div>
            <Button type="submit" variant="contained" color="primary" className={classes.button}>
              Login
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

Login.propTypes = { //mengirimkan beberapa validator yang dapat digunakan
  classes: PropTypes.object.isRequired,
};

//memanggil parameter yang sudah dideclared kapanpun ada perubahan
//react destructure props
//dari props state punya objek data curdStudent
//curdStudent punya data dengan nama credentials
//agar dapat mengakses credentials lalu yang dikembalikan dari mapStateToProps adalah credentials
function mapStateToProps(state) {
  const { credentials } = state.crudStudent;
  return {
    credentials
  };
}
//mengekspor material-ui styles dengan konektor redux
export default connect(mapStateToProps)(withStyles(styles)(Login));