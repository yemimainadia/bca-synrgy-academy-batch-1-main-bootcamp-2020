export const menu = [
    {
        id: '1',
        name: 'Report',
        url: '/dashboard/backoffice',
        icon: 'DesktopOutlined',
        visibility: ['1', '2', '3']
    },
    {
        id: '2',
        name: 'Content',
        url: '/dashboard/content',
        icon: 'PieChartOutlined',
        visibility: ['1']
    },
    {
        id: '3',
        name: 'Marketing',
        url: '/dashboard/marketing',
        icon: 'PieChartOutlined',
        visibility: ['1', '3']
    },
    {
        id: '4',
        name: 'Customer',
        url: '/dashboard/customer',
        icon: 'PieChartOutlined',
        visibility: ['1', '2']
    },
    {
        id: '5',
        name: 'Stores',
        url: '/dashboard/stores',
        icon: 'PieChartOutlined',
        visibility: ['2']
    }
]