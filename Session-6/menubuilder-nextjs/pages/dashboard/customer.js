import { Layout, Menu, Breadcrumb } from 'antd';
import Router from 'next/router';
import Profile from './profile';
import Sidebar from './sidebar';
import Report from '../dashboard/report';
import React, { useState, useEffect } from "react";
import CustomerData from '../dashboard/customer-data';
import {
    UserOutlined
} from '@ant-design/icons';


const { Header, Content, Footer } = Layout;

const Backoffice = () => {
    const [login, setLogin] = useState(false);

    useEffect(() => {
        const currentUser = localStorage.getItem('adminuser');
        if (currentUser === null) {
            setLogin(false)
            Router.push('/');
        } else {
            setLogin(true);
        }
    }, []);

    // const currentUser = localStorage.getItem('username');
    return (
        <Layout style={{ minHeight: '100vh', display: login ? 'flex' : 'none' }}>
            <Sidebar />
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }}>
                    <Profile />
                    <div className="icon-admin"> {<UserOutlined />}</div>
                </Header>
                <Content style={{ margin: '0 16px' }}>
                    <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                        <CustomerData />
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>©2020 All Rights Reserved</Footer>
            </Layout>
        </Layout>
    );
};
export default Backoffice;