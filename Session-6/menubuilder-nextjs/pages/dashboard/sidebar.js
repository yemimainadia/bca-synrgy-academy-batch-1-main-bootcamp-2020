import Link from 'next/link';
import { Layout, Menu, Breadcrumb } from 'antd';
import {
    TableOutlined,
    LineChartOutlined,
    TeamOutlined,
    ShoppingCartOutlined,
    ShopOutlined
} from '@ant-design/icons';

const { Sider } = Layout;
const { SubMenu } = Menu;

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menunav: [],
            menunavActive: 1
        }
    }
    state = {
        collapsed: false,
    };

    onCollapse = collapsed => {
        console.log(collapsed);
        this.setState({ collapsed });
    };

    componentDidMount() {
        const adminSession = localStorage.getItem('adminuser');
        if (adminSession) {
            fetch(`/api/menu-collection/${adminSession}`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(response => response.json())
                .then((jsonData) => {
                    if (jsonData) {
                        let currentactivemenu;
                        let menunavigation = jsonData.map((menu) => {
                            if (menu.url == window.location.pathname) {
                                currentactivemenu = menu.id;
                            }
                            return (
                                <Menu.Item key={menu.id}>
                                    <Link href={menu.url}>
                                        <a>{menu.name}</a>
                                    </Link>
                                </Menu.Item>
                            )
                        });
                        this.setState({ menunavActive: currentactivemenu });
                        this.setState({ menunav: menunavigation });
                        console.log("state", this.state.menunav);
                        console.log("current menu active", this.state.menunavActive);
                    }
                })
        }
    }


    render() {
        console.log(this.state.menunavActive)
        return (
            <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
                <div className="header" />
                <Menu theme="dark" selectedKeys={`${this.state.menunavActive}`} mode="inline">
                    {this.state.menunav}
                </Menu>
            </Sider>
        );
    }
}
export default Sidebar;