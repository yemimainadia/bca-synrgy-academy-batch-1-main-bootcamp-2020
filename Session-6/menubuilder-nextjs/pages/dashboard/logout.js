import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import Router from 'next/router';
import { route } from 'next/dist/next-server/server/router';


class DropdownProfile extends React.Component {
    state = {
        visible: false,
    };

    handleMenuClick = e => {
        if (e.key === '2') {
            localStorage.removeItem('adminuser');
            Router.push('/')
            this.setState({ visible: false });
        }
    };

    handleVisibleChange = flag => {
        this.setState({ visible: flag });
    };

    render() {
        const menu = (
            <Menu onClick={this.handleMenuClick} >
                <Menu.Item key="1">Edit Profile</Menu.Item>
                <Menu.Item key="2">Logout</Menu.Item>
            </Menu>
        );
        return (
            <Dropdown
                overlay={menu}
                onVisibleChange={this.handleVisibleChange}
                visible={this.state.visible}
            >
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                    Admin <DownOutlined />
                </a>
            </Dropdown>
        );
    }
}

export default DropdownProfile;