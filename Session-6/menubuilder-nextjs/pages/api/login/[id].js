import { admin } from '../../../data/admin';

export default function adminLoginHandler(req, res) {
    const { method } = req;
    let filteredAdmin;
    switch (method) {
        case 'GET':
            break;
        case 'POST':
            const urlHandle = req.query.id;
            if (urlHandle == 'admin') {
                const username = req.body.username;
                const password = req.body.password;

                //filter data user yang sesuai dengan form login yang diinput
                filteredAdmin = admin.filter((p) => p.username === username && p.password === password);
                if (filteredAdmin.length > 0) {
                    res.status(200).json({ data: filteredAdmin[0]['id'] }); //jika ketemu, maka akan dikirimkan user id nya
                } else {
                    res.status(202).json({ error: 'Username and or Password is not found.' })
                }
            }
            break;
        default:
            res.setHeader('Allow', ['GET', 'POST'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}