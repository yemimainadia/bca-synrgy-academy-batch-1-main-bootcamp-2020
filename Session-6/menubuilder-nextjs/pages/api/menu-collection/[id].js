import { menu } from '../../../data/menu-collection';

export default function menunavigationHandler(req, res) {
    const { method } = req;
    let filteredAdmin;
    switch (method) {
        case 'GET':
            const adminID = req.query.id;

            //filter menu yang boleh dilihat berdasarkan id dari user yang sudah login
            filteredAdmin = menu.filter((m) => m.visibility.includes(adminID));
            console.log(filteredAdmin);
            if (filteredAdmin.length > 0) {
                res.status(200).json(filteredAdmin); //jika ketemu maka dikirimkan object menu-menu yang boleh dilihat
            } else {
                res.status(202).json({ error: `ga ada` });
            }
            break
        default:
            res.setHeader('Allow', ['GET', 'POST'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}