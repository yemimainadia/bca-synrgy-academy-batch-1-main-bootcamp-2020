import Head from "next/head";

import { Layout } from 'antd';
import Sider from "../components/sider";
import Breadcrumbs from "../components/breadcrumbs";
import Usermenu from "./usermenu";


export default function LayoutPage({ children, pageConfig }) {
    const { title } = pageConfig;
    const { menuSelected } = pageConfig;

    const routes = [
        {
            path: 'index',
            breadcrumbName: title,
        }
    ];

    return (
        <>
            <Head>
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <link rel="manifest" href="/site.webmanifest" />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffffff" />
                <title>Page: {title} </title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <Layout>
                <Sider callbackMenuSelected={menuSelected} />
                <Layout className="site-layout">
                    <Breadcrumbs routes={routes} />
                    <Usermenu />
                    {children}
                </Layout>
            </Layout>
        </>
    );
}
