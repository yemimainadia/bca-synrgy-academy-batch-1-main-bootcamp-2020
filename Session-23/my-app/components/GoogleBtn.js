import React, { Component } from 'react'
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import Router from "next/router";
import { GoogleClientID } from "./constGoogle";

const CLIENT_ID = GoogleClientID;

const store = require('store');

const customStyle = {
    backgroundColor: '#88BDBC'
}


class GoogleBtn extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLogined: false,
            accessToken: ''
        };

        this.login = this.login.bind(this);
        this.handleLoginFailure = this.handleLoginFailure.bind(this);
        this.logout = this.logout.bind(this);
        this.handleLogoutFailure = this.handleLogoutFailure.bind(this);
    }


    login(response) {
        let responseDataProfile = response.profileObj;
        if (response.accessToken) {
            this.setState(state => ({
                isLogined: true,
                accessToken: response.accessToken
            }));
            console.log(responseDataProfile);
            // set localstorage
            store.set('apps-storage', { user: { email: responseDataProfile.email, isGoogle: true, name: responseDataProfile.name } });
            Router.push('/dashboard');
        }
    }

    logout(response) {
        this.setState(state => ({
            isLogined: false,
            accessToken: ''
        }));
    }

    handleLoginFailure(response) {
        console.log(response)
        alert('Failed to log in')
    }

    handleLogoutFailure(response) {
        console.log(response)
        alert('Failed to log out')
    }

    render() {
        return (
            <div className="container-google">
                {this.state.isLogined ?
                    <GoogleLogout
                        clientId={CLIENT_ID}
                        buttonText='Logout'
                        onLogoutSuccess={this.logout}
                        onFailure={this.handleLogoutFailure}
                        style={customStyle}
                    >
                    </GoogleLogout> : <GoogleLogin
                        clientId={CLIENT_ID}
                        buttonText='Sign in with Google'
                        onSuccess={this.login}
                        onFailure={this.handleLoginFailure}
                        cookiePolicy={'single_host_origin'}
                        responseType='code,token'
                        className="btn-google"
                        style={customStyle}
                    />
                }
                {/*this.state.accessToken ? <h5>Your Access Token: <br /><br /> {this.state.accessToken}</h5> : null*/}
            </div>
        )
    }
}

export default GoogleBtn;