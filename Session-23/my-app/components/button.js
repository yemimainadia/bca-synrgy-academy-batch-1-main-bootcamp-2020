import { Button } from 'antd';

import React from 'react';

const ButtonComponent = () => {
    return (
        <Button className="btn-table" type="primary">Action Button</Button>
    );
}

export default ButtonComponent;
