import { PageHeader } from 'antd';

const Breadcrumbs = ({routes}) => {
    return (
        <PageHeader
            className="site-page-header"
            breadcrumb={{ routes }}
        />

    );
}

export default Breadcrumbs;