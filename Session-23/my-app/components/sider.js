import React from 'react';
import Link from 'next/link';
import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    SoundFilled,
    ContactsFilled,
    LayoutFilled,
    ScheduleFilled,
    MedicineBoxFilled,
    AudioFilled
} from '@ant-design/icons';


const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu;

class Siderbar extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <Sider trigger={null} collapsible collapsed={this.state.collapsed}>

                <Header className="site-layout-background" style={{ padding: 0 }}>
                    {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: this.toggle,
                    })}
                    <div className="logo" >
                        <img className="logo-img" src="/assets/logo.png" />
                    </div>
                </Header>

                <Menu theme="dark" mode="inline" selectedKeys={`${this.props.callbackMenuSelected}`}>
                    <h1 className="sider-h1">My Hospital</h1>
                    <Menu.Item key="1" icon={<LayoutFilled />}>
                        <Link href="/dashboard">
                            Dashboard
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<AudioFilled />}>
                        <Link href="/dictation">
                            Dictation
                         </Link>
                    </Menu.Item>
                    <Menu.Item key="3" icon={<ContactsFilled />}>
                        <Link href="/users">
                            Users
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="4" icon={<ScheduleFilled />}>
                        <Link href="/appointments">
                            Appointments
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="5" icon={<MedicineBoxFilled />}>
                        <Link href="/symptoms">
                            Symptoms
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="6" icon={<SoundFilled />}>
                        <Link href="/alerts">
                            Alerts
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }

};

export default Siderbar;


