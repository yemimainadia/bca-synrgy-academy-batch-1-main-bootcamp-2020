import React from 'react';
import { Menu, Dropdown } from 'antd';
import { UserOutlined, LogoutOutlined } from '@ant-design/icons';

import { GoogleLogout } from 'react-google-login';
import { GoogleClientID } from "./constGoogle";
import Router from "next/router";

const store = require('store');

const Usermenu = () => {
    const handleMenuCustomerClick = (e) => {
        if (e.key == 1) {
            clearAllSession();
        }
    }
    const logoutGoogle = () => {
        clearAllSession();
    }
    const clearAllSession = () => {
        store.clearAll();
        Router.push('/');
    }

    const userInfo = store.get('apps-storage') ? store.get('apps-storage').user.name : "";

    const menu = () => {
        if (store.get('apps-storage') && store.get('apps-storage').user.isGoogle) {
            return (
                <GoogleLogout
                    clientId={GoogleClientID}
                    buttonText="Logout"
                    onLogoutSuccess={logoutGoogle}
                />
            )
        } else {
            return (
                <Menu onClick={handleMenuCustomerClick}>
                    <Menu.Item key="1" icon={<LogoutOutlined />}>
                        Logout
                    </Menu.Item>
                </Menu>
            )
        }
    };


    return (
        <Dropdown.Button overlay={menu} icon={<UserOutlined />} className="user-greetings">
            <a href='/profile'> <span>Welcome, {userInfo} </span> </a>

        </Dropdown.Button>
    );
}

export default Usermenu;
