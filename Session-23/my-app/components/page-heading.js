import React from 'react';

const Pageheading = ({pageConfig}) => {
    const { title } = pageConfig;
    return (
        <div>
            <h1 className="page-title">{title}</h1>
        </div>
    );
}

export default Pageheading;
