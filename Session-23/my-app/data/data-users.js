export const dataUsers = [
    {
        id: "user-1",
        name: "Bonnie Wright",
        username: "bonnie_",
        email: "bonnie@mail.com",
        roles: ["manager"],
        group: "small"
    },
    {
        id: "user-2",
        name: "Bardi Subardi",
        username: "bardi_",
        email: "bardi@mail.com",
        roles: ["doctor"],
        group: "medium"
    },
    {
        id: "user-3",
        name: "Coco Marico",
        username: "coco_",
        email: "coco@mail.com",
        roles: ["doctor"],
        group: "large"
    },
    {
        id: "user-4",
        name: "Michele Kanwald",
        username: "michele_",
        email: "michele@mail.com",
        roles: ["patient"],
        group: "medium"
    },
    {
        id: "user-5",
        name: "Patricia Cilis",
        username: "cilis_",
        email: "cilis@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-6",
        name: "Barney Karovsky",
        username: "barney_",
        email: "barney@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-7",
        name: "Laila Sari",
        username: "laila_",
        email: "laila@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-8",
        name: "Molly Weasley",
        username: "molly_",
        email: "molly@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-9",
        name: "John Doe",
        username: "john_",
        email: "john@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-10",
        name: "Jean Rise",
        username: "jean_",
        email: "jean@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-11",
        name: "Gigi Hadid",
        username: "gigi_",
        email: "gigi@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-12",
        name: "Bella Hadid",
        username: "bella_",
        email: "bella@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-13",
        name: "Kendall Jenner",
        username: "kendall_",
        email: "kendall@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-14",
        name: "Behati Prinsloo",
        username: "behati_",
        email: "behati@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-15",
        name: "Kylie Jenner",
        username: "kylie_",
        email: "kylie@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-16",
        name: "Kiko Mizuhara",
        username: "kiko_",
        email: "kiko@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-17",
        name: "Leona Young",
        username: "leona_",
        email: "leona@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-18",
        name: "Usa Usa",
        username: "usa_",
        email: "usa@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-19",
        name: "Olive Rabbit",
        username: "olive_",
        email: "olive@mail.com",
        roles: ["patient"],
        group: "small"
    },
    {
        id: "user-20",
        name: "Rose Horton",
        username: "rose_",
        email: "rose@mail.com",
        roles: ["patient"],
        group: "small"
    }

];