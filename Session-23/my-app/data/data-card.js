export const data = [
    {
        id: "1",
        classStyle: "card-users",
        name: "Card User",
        title: "New Users",
        value_key: 1098,
        value_day: 10,
        statistic: [
            {
                name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
            },
            {
                name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
            },
            {
                name: 'Page C', uv: 2000, pv: 9800, amt: 2290,
            },
            {
                name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
            },
            {
                name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
            },
            {
                name: 'Page F', uv: 2390, pv: 10000, amt: 2500,
            },
            {
                name: 'Page G', uv: 3490, pv: 12000, amt: 2100,
            },
        ]
    },

    {
        id: "2",
        classStyle: "card-appointments",
        name: "Card Appointment",
        title: "Appoinments",
        value_key: 31,
        value_day: 10,
        statistic: [
            {
                name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
            },
            {
                name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
            },
            {
                name: 'Page C', uv: 2000, pv: 3000, amt: 2290,
            },
            {
                name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
            },
            {
                name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
            },
            {
                name: 'Page F', uv: 2390, pv: 3800, amt: 2500,
            },
            {
                name: 'Page G', uv: 3490, pv: 4300, amt: 2100,
            },
        ]
    },

    {
        id: "3",
        classStyle: "card-alerts",
        name: "Card Alerts",
        title: "Alerts",
        value_key: 301,
        value_day: 111,
        statistic: [
            {
                name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
            },
            {
                name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
            },
            {
                name: 'Page C', uv: 2000, pv: 3000, amt: 2290,
            },
            {
                name: 'Page D', uv: 2780, pv: 5000, amt: 2000,
            },
            {
                name: 'Page E', uv: 1890, pv: 10000, amt: 2181,
            },
            {
                name: 'Page F', uv: 2390, pv: 15000, amt: 2500,
            },
            {
                name: 'Page G', uv: 3490, pv: 20000, amt: 2100,
            },
        ]

    },

    {
        id: "4",
        classStyle: "card-dictations",
        name: "Card Dictations",
        title: "Dictations",
        value_key: 77,
        value_day: 42,
        statistic: [
            {
                name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
            },
            {
                name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
            },
            {
                name: 'Page C', uv: 2000, pv: 2000, amt: 2290,
            },
            {
                name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
            },
            {
                name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
            },
            {
                name: 'Page F', uv: 2390, pv: 3800, amt: 2500,
            },
            {
                name: 'Page G', uv: 3490, pv: 3000, amt: 2100,
            },
        ]
    }
];