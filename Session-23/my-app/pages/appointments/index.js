import React from "react";
import { Layout } from 'antd';

import LayoutPage from "../../components/layoutpage";
import Footer from "../../components/footer";
import Pageheading from "../../components/page-heading";

const { Content } = Layout;

const Index = () => {
    const pageConfig = {
        title: "Appointments",
        menuSelected: 4
    };

    return (
        <main>
            <LayoutPage pageConfig={pageConfig}>
                <Content
                    className="site-layout-background"
                    style={{
                        padding: 24
                    }}
                >
                    <Pageheading pageConfig={pageConfig} />
                </Content>
            </LayoutPage>
            <Footer />
        </main>
    );
};

export default Index;