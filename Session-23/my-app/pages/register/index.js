import React, { useState, useEffect } from "react";
import { Form, Input, Button } from 'antd';
import { MailOutlined, LockOutlined, UserOutlined } from '@ant-design/icons';
import Router from "next/router";
import LoginLoader from "../../components/loginloader";

const store = require('store');

const RegisterForm = () => {
    const [pageLoad, setPageLoad] = useState(true);


    const onFinish = values => {
        localStorage.setItem('user', values.email);
        // set localstorage
        store.set('apps-storage', { customer: { email: values.email } });
        Router.push('/dashboard');
    };

    const ContentPage = () => {
        setTimeout(function () { //Start the timer
            setPageLoad(false) //After 1 second, set to false
        }, 1000);

        if (pageLoad) {
            return (
                <div className="loader-login">
                    <LoginLoader />
                </div>
            )
        } else {
            return (
                <div className="background-container-login">
                    <div className="container-login">
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                        >
                            <div className="logo" >
                                <img className="logo-img" src="/assets/logo.png" />
                            </div>
                            <h1 className="title-login">Sign Up</h1>
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Name!',
                                    },
                                ]}
                            >
                                <Input prefix={<UserOutlined className="login-icon" />} placeholder="Name" />
                            </Form.Item>
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Email!',
                                    },
                                ]}
                            >
                                <Input prefix={<MailOutlined className="login-icon" />} placeholder="Email address" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Password!',
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<LockOutlined className="login-icon" />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Sign Up
                            </Button>
                                <p className="register-caption">Already have an account? <a className="register-link" href="/login">Sign In here</a></p>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            )
        }
    }

    return (
        <ContentPage />
    );
}

export default RegisterForm;