import { Layout, Menu } from 'antd';
import HeaderComponent from './header';
import CardComponent from './card';
import PageTitle from './page-title';
import UsersContent from './content/users';
import Siderbar from '../../components/sider';

const { SubMenu } = Menu;
const { Content } = Layout;

class LayoutDashboard extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (

            <Layout>
                <Siderbar />
                <Layout className="site-layout">
                    <HeaderComponent />
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            minHeight: 280,
                        }}
                    >
                        <CardComponent />
                        <PageTitle />
                        <UsersContent />
                    </Content>
                </Layout>
            </Layout>

        );
    }
};

export default LayoutDashboard;