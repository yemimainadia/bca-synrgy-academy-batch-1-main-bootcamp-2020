import { Table, Tag, Radio, Space } from 'antd';
import { dataUsers } from '../../../data/data-users';

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
    },
    {
        title: 'Username',
        dataIndex: 'username',
        key: 'username',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: 'Role',
        key: 'role',
        dataIndex: 'roles',
        render: roles => (
            <span>
                {roles.map(role => {
                    let color = role.length > 5 ? 'geekblue' : 'green';
                    if (role === 'manager') {
                        color = 'volcano';
                    }
                    if (role === 'doctor') {
                        color = 'blue';
                    }
                    if (role === 'patient') {
                        color = 'green';
                    }
                    return (
                        <Tag color={color} key={role}>
                            {role.toUpperCase()}
                        </Tag>
                    );
                })}
            </span>
        ),
    },
    {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
            <Space size="middle">
                <a>Details</a>
                <a>Delete</a>
            </Space>
        ),
    },
];


class Users extends React.Component {
    state = {
        bottom: 'bottomRight',
    };

    render() {
        return (
            <div>
                <div>
                    <Table
                        columns={columns}
                        pagination={{ position: [this.state.bottom] }}
                        dataSource={dataUsers}
                    />
                </div>
            </div>
        );
    }
}

export default Users;