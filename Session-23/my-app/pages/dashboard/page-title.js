import React from 'react';

const PageTitle = () => {
    return (
        <div>
            <h1 className="page-title">Users</h1>
        </div>
    );
}

export default PageTitle;
