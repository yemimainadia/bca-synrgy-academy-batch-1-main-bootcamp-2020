import React, { useEffect } from "react";
import { Layout } from 'antd';

import LayoutPage from "../../components/layoutpage";
import Footer from "../../components/footer";
import DashboardLoader from "../../components/dashboardloader";

import CardComponent from './card';
import UsersContent from './users';
import Pageheading from "../../components/page-heading";

import Router from "next/router";
import ContentLoader from "react-content-loader";

const store = require('store');

const { Content } = Layout;

const Index = () => {

    useEffect(() => {
        if (store.get('apps-storage') && store.get('apps-storage').user.email) {
            Router.push('/dashboard');
        } else {
            Router.push('/');
        }
    }, []);

    const pageConfig = {
        title: "Dashboard",
        menuSelected: 1
    };

    const ContentPage = () => {
        if (store.get('apps-storage') && store.get('apps-storage').user.email) {
            return (
                <main>
                    <LayoutPage pageConfig={pageConfig}>
                        <Content
                            className="site-layout-background"
                            style={{
                                padding: 24,
                                minHeight: 280,
                            }}
                        >
                            <CardComponent />
                            <Pageheading pageConfig={pageConfig} />
                            <UsersContent />
                        </Content>
                    </LayoutPage>
                    <Footer />
                </main>
            )
        } else {
            return (
                <main>
                    <DashboardLoader />
                </main>
            )
        }
    }
    return (
        <ContentPage />
    );
};

export default Index;