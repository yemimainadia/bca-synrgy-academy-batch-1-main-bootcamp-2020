import { Card, Col, Row, Statistic } from 'antd';
import {
    AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';

import { data } from '../../data/data-card';


const Cards = () => {
    return (
        <div className="site-card-wrapper">
            <Row gutter={24}>
                {data.map((card_data) =>
                    <Col span={6}>
                        <Card className={card_data.classStyle} bordered={false}>
                            <Row gutter={16}>
                                <Col span={12}>
                                    <Statistic className="new-users-card" title={card_data.title} value={card_data.value_key} />
                                </Col>
                                <Col span={12}>
                                    <Statistic className="today-card" value={card_data.value_day} title="Today" />
                                </Col>
                            </Row>
                            <AreaChart
                                width={250}
                                height={60}
                                data={card_data.statistic}
                                margin={{
                                    top: 5, right: 0, left: 0, bottom: 24,
                                }}>
                                <defs>
                                    <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1.5">
                                        <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                                        <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
                                    </linearGradient>
                                </defs>
                                <Area type="monotone" dataKey="pv" stroke="#FFD064" fillOpacity={1} fill="url(#colorPv)" />
                            </AreaChart>
                        </Card>
                    </Col>
                )}
            </Row>
        </div>
    );
}

export default Cards;