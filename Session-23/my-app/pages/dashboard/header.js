import { PageHeader } from 'antd';

const routes = [
    {
        path: 'index',
        breadcrumbName: 'Dashboard',
    },
    {
        path: 'first',
        breadcrumbName: 'Active Breadcrumb',
    }
];


const Header = () => {
    return (
        <PageHeader
            className="site-page-header"
            breadcrumb={{ routes }}
        />

    );
}

export default Header;