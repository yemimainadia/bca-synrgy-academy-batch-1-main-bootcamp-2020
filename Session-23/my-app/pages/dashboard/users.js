import { Table, Tag, Radio, Space } from 'antd';
import { dataUsers } from '../../data/data-users';
import ButtonComponent from '../../components/button';

const roleCollection = [];
dataUsers.forEach(function (data) {
    let stringRole = JSON.stringify(data['roles']);
    let cleanupRole1 = stringRole.replace("[", "");
    let cleanupRole2 = cleanupRole1.replace("]", "");
    let cleanupRole = cleanupRole2.replace(/['"]+/g, '');
    roleCollection.push(cleanupRole);
});
const uniqueRolesCollection = Array.from(new Set(roleCollection));

const filterRoles = [];
uniqueRolesCollection.forEach(function (data) {
    filterRoles.push({
        text: data,
        value: data
    });
});
console.log(filterRoles);

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.length - b.name.length,
        sortDirections: ['descend', 'ascend'],
        render: text => <a>{text}</a>,
    },
    {
        title: 'Username',
        dataIndex: 'username',
        key: 'username',
        sorter: (a, b) => a.username.length - b.username.length,
        sortDirections: ['descend', 'ascend'],
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        sorter: (a, b) => a.email.length - b.email.length,
        sortDirections: ['descend', 'ascend'],
    },
    {
        title: 'Role',
        key: 'role',
        dataIndex: 'roles',
        render: roles => (
            <span>
                {roles.map(role => {
                    let color = role.length > 5 ? 'geekblue' : 'green';
                    if (role === 'manager') {
                        color = 'volcano';
                    }
                    if (role === 'doctor') {
                        color = 'blue';
                    }
                    if (role === 'patient') {
                        color = 'green';
                    }
                    return (
                        <Tag color={color} key={role}>
                            {role.toUpperCase()}
                        </Tag>
                    );
                })}
            </span>
        ),
        filters: filterRoles,
        // specify the condition of filtering result
        // here is that finding the name started with `value`
        onFilter: (value, record) => record.roles.indexOf(value) === 0,
    },
    {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
            <ButtonComponent />
        ),
    },
];


class Users extends React.Component {
    state = {
        bottom: 'bottomRight',
    };

    render() {
        return (
            <div>
                <div>
                    <Table
                        columns={columns}
                        pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30'] }}
                        dataSource={dataUsers}
                    />
                </div>
            </div>
        );
    }
}

export default Users;