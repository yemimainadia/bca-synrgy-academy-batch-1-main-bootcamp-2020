import React, { useState, useEffect } from "react";
import { Form, Input, Button } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import GoogleBtn from '../components/GoogleBtn';
import Divider from '../components/divider';
import Head from "next/head";

import Router from "next/router";
import LoginLoader from "../components/loginloader";

const store = require('store');

const LoginForm = () => {
    const [pageLoad, setPageLoad] = useState(true);
    useEffect(() => {
        if (store.get('apps-storage') && store.get('apps-storage').user.email) {
            Router.push('/dashboard');
        } else {
            Router.push('/');
        }
    }, []);

    const onFinish = values => {
        if (values.email === "admin@mail.com" && values.password === "admin123") {
            // set localstorage
            store.set('apps-storage', { user: { email: values.email, name: 'Admin' } });
            Router.push('/dashboard');
            //localStorage.setItem('user', values.email);
        } else {
            alert("Proceed to admin only")
        }


    };

    const ContentPage = () => {
        setTimeout(function () { //Start the timer
            setPageLoad(false) //After 1 second, set to false
        }, 1000);

        if (pageLoad) {
            return (
                <div className="loader-login">
                    <LoginLoader />
                </div>
            )
        } else {
            return (

                <div className="background-container-login">
                    <div className="container-login">
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                        >
                            <div className="logo" >
                                <img className="logo-img" src="/assets/logo.png" />
                            </div>
                            <h1 className="title-login">Login</h1>
                            <GoogleBtn />
                            <Divider />
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Email!',
                                    },
                                ]}
                            >
                                <Input prefix={<MailOutlined className="login-icon" />} placeholder="Email address" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Password!',
                                    },
                                ]}
                            >
                                <Input
                                    prefix={<LockOutlined className="login-icon" />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Login
                            </Button>

                                <p className="register-caption">Don't have an account? <a className="register-link" href="/register">Register now</a></p>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            )
        }
    }

    return (
        <>
            <Head>
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <link rel="manifest" href="/site.webmanifest" />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffffff" />
                <title>Login</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <ContentPage />
        </>
    );
}

export default LoginForm;