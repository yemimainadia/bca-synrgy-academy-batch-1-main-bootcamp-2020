import React from 'react';
import { Form, Input, InputNumber, Button } from 'antd';

const layout = {
    labelCol: {
        div: 8,
    },
    wrapperCol: {
        div: 16,
    },
};
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not validate email!',
        number: '${label} is not a validate number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

const CardForm = () => {

    const onFinish = values => {
        console.log(values);
    };

    return (
        <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
            <Form.Item
                className="email-input"
                name={['user', 'email']}
                label="Email"
                rules={[
                    {
                        type: 'email',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                className="username-input"
                name={['user', 'username']}
                label="Username"
                rules={[
                    {
                        type: 'username',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                className="first-name-input"
                name={['user', 'firstname']}
                label="First Name"
                rules={[
                    {
                        type: 'firstname',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                className="last-name-input"
                name={['user', 'lastname']}
                label="Last Name"
                rules={[
                    {
                        type: 'lastname',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                className="address-input"
                name={['user', 'address']}
                label="Address"
                rules={[
                    {
                        type: 'address',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item className="description-input" name={['user', 'description']} label="Description">
                <Input.TextArea />
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button className="update-profile" type="primary" htmlType="submit">
                    Update Profile
        </Button>
            </Form.Item>
        </Form>
    );
}

export default CardForm;
