import React from 'react';
import AvatarComponent from './card-avatar';
import Footer from "../../components/footer";
import LayoutPage from "../../components/layoutpage";
import Pageheading from "../../components/page-heading";
import { Layout } from 'antd';

const { Content } = Layout;

const pageConfig = {
    title: "Profile",
    menuSelected: 1
};
const ProfilePage = () => {
    return (
        <main>

            <LayoutPage pageConfig={pageConfig}>
                <Content
                    className="site-layout-background"
                    style={{
                        padding: 24,
                        minHeight: 754,
                    }}
                >
                    <Pageheading pageConfig={pageConfig} />
                    <AvatarComponent />
                </Content>
            </LayoutPage>
            <Footer />

        </main>
    );
}

export default ProfilePage;
