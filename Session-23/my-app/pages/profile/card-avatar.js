import React from 'react';
import { Card, Avatar, Col, Row } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import UploadAvatar from './upload-avatar';
import FormEdit from './card-form';

const { Meta } = Card;


const CardAvatar = () => {

    return (
        <div className="site-card-wrapper">
            <Row gutter={16}>
                <Col span={8}>
                    <div className="img-wrapper">
                        <UploadAvatar />
                    </div>
                </Col>
                <Col span={16}>
                    <Card title="Account Information">
                        <FormEdit />
                    </Card>
                </Col>
            </Row>
        </div>
    );

}

export default CardAvatar;
