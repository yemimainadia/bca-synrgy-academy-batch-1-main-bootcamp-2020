import ReactCrop from 'react-image-crop';
import React, { useState } from 'react';
import { Card, Avatar } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import ProgressBar from './progress';

const store = require('store');
const { Meta } = Card;

const userInfo = store.get('apps-storage') ? store.get('apps-storage').user.name : "";
const emailInfo = store.get('apps-storage') ? store.get('apps-storage').user.email : "";

class UploadAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: null,
            crop: {
                unit: '%',
                width: 50,
                aspect: 1 / 1,
            },
            avatarConfigured: false,
            userAvatar: ''
        };
    }

    componentDidMount() {
        this._getAvatarProfile();
    }

    _getAvatarProfile = (e) => {
        let avatar = store.get('apps-storage').user.avatar ? store.get('apps-storage').user.avatar : 'https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png';
        this.setState({ userAvatar: avatar });
    }

    onSelectFile = e => {
        this.setState({ avatarConfigured: false });
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            const userBasicInfo = store.get('apps-storage').user;
            const userBasicInfoWithAvatar = {avatar: croppedImageUrl};
            store.set('apps-storage', { user: { ...userBasicInfo,...userBasicInfoWithAvatar } });
            this.setState({ croppedImageUrl });            
            this.setState({ userAvatar: croppedImageUrl });
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
        const base64Image = canvas.toDataURL('image/jpeg');
        return base64Image;
    }

    avatarSaved = () => {
        this.setState({ avatarConfigured: true });
    }

    triggerInputToClick = () => {
        this.inputElement.click();
    }

    render() {
        return (
            <div className="avatar-crop-wrapper">
                <div className="avatar-form">
                    <form id="formSetAvatar" onSubmit={this.avatarSaved} className="form-set-avatar">
                        <fieldset>
                            <input id="newAvatar" type="file" accept="image/*" onChange={this.onSelectFile} ref={input => this.inputElement = input} />
                            <a href="javascript:;" onClick={this.triggerInputToClick}><EditOutlined /></a>
                        </fieldset>
                    </form>                    
                        <Card
                            cover={
                                <img alt="user-avatar" className="avatar-upload" style={{ maxWidth: '100%' }} src={this.state.userAvatar} />
                            }

                            actions={[
                                <>
                                    <h1 className="workload">Workload</h1>
                                    <ProgressBar />
                                    <h3 className="notes-card">Notes:</h3>
                                    <div className="desc-workload">
                                        <p>Ready for promotion</p>
                                        <p>Workload last week 100%</p>
                                    </div>
                                </>
                            ]}
                        >
                            <Meta
                                title={userInfo}
                                description={emailInfo}
                            />
                        </Card>                    
                </div>
                <div className="avatar-preview">
                    {this.state.src && !this.state.avatarConfigured ? (
                        <>
                            <ReactCrop
                                src={this.state.src}
                                crop={this.state.crop}
                                ruleOfThirds
                                onImageLoaded={this.onImageLoaded}
                                onComplete={this.onCropComplete}
                                onChange={this.onCropChange}
                            />
                            <button type="button" onClick={this.avatarSaved}>Set My Avatar!</button>
                        </>
                    ) : null}
                </div>
            </div>
        );
    }
}

export default UploadAvatar;