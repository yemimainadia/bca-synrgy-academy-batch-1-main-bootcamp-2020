import React from 'react';
import { Progress } from 'antd';

const ProgressBar = () => {
    return (
        <Progress className="progress-work" type="circle" percent={30} width={80} />
    );
}

export default ProgressBar;
