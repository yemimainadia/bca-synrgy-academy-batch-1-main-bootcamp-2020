import { Divider } from 'antd';

import React from 'react';

const DividerComponent = () => {
    return (
        <Divider>or</Divider>
    );
}

export default DividerComponent;

