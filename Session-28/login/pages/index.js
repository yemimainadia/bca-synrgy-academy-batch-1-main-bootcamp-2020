import Head from 'next/head'
import styles from '../styles/Home.module.css';
import Login from './login';
import LoginForm from './login';


export default function Home() {
  return (
    <main>
      <Login />
      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </main>
  )
}
