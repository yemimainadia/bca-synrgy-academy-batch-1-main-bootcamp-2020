import { Form, Input, Button, Checkbox } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import GoogleLogin from 'react-google-login';
import GoogleBtn from './components/GoogleBtn';
import Divider from './components/divider';

import React from 'react';

const LoginForm = () => {
    const onFinish = values => {
        console.log('Received values of form: ', values);
    };



    return (
        <div className="container-login">
            <Form
                name="normal_login"
                className="login-form"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
            >
                <div className="logo-login" >
                    <img className="logo-img" src="/assets/logo.png" />
                </div>
                <h1 className="title-login">Login</h1>
                <GoogleBtn />
                <Divider />
                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input prefix={<MailOutlined className="login-icon" />} placeholder="Email address" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input
                        prefix={<LockOutlined className="login-icon" />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Login
                    </Button>

                    <p className="register-caption">Don't have an account? <a className="register-link" href="">Register now</a></p>
                </Form.Item>
            </Form>
        </div>
    );
}

export default LoginForm;