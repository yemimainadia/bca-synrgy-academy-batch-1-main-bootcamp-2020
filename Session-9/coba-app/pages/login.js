import { Form, Input, Button, Checkbox } from 'antd';
import Router from 'next/router';
import React, { useState, useEffect } from "react";

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};

const Login = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    useEffect(() => {
        const currentUser = localStorage.getItem('adminuser');
        if (currentUser) {
            Router.push('/login');
        }
    }, []);

    const onFinishFormLogin = (values) => {
        // fetch(`/api/login/admin`, {
        //     method: 'POST',
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         username: username,
        //         password: password,
        //     })
        // }).then(response => response.json())
        // .then((jsonData) => {
        //     // jsonData is parsed json object received from url
        //     if(jsonData['error']) {
        //         alert(jsonData['error']);
        //     } else {
        //         Router.push('/dashboard');
        //         localStorage.setItem('adminuser', jsonData['data']);
        //     }
        // });
        console.log(username);
        if (username == 'admin') {

        } else if (username == 'operator') {

        } else {

        }
    };


    const onValuesChangeFormLogin = values => {
        if (values.username) {
            setUsername(values.username);
        }
        if (values.password) {
            setPassword(values.password);
        }
    };

    return (
        <Form
            {...layout}
            name="basic"
            initialValues={{
                remember: true,
            }}
            onFinish={onFinishFormLogin}
            onValuesChange={onValuesChangeFormLogin}

        >
            <Form.Item
                label="Username"
                name="username"
                rules={[
                    {
                        required: true,
                        message: 'Please input your username!',
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
        </Button>
            </Form.Item>
        </Form>
    );
};

export default Login;