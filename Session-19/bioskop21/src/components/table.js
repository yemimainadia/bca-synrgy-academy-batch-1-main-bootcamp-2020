import React, { useState, useEffect } from "react";
import API from "../api";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";
import Modal from "react-bootstrap/Modal";
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const Dashboard = () => {
    const [moviedata, setMoviedata] = useState([]);
    const [roleid, setRoleid] = useState("");
    const [isInputReadonly, setInputReadonly] = useState(true);

    const [showEditForm, setShowEditForm] = useState(false);

    const [editMovieID, setEditMovieID] = useState('');
    const [editTitle, setEditTitle] = useState('');
    const [editSynopsis, setEditSynopsis] = useState('');
    const [editGenre, setEditGenre] = useState('');
    const [editPoster, setEditPoster] = useState('');
    const [editStatus, setEditStatus] = useState('');
    const [defaultMovieStatus, setDefaultMovieStatus] = useState('pending');

    useEffect(() => {
        getMovieList();
        if (localStorage.getItem('token-roleID')) {
            setRoleid(localStorage.getItem('token-roleID'))
        }
    }, []);

    const getMovieList = () => {
        API.get("movies?max=10").then(({ data }) => {
            setMoviedata(data);
        });
    };

    const deleteMovie = (movieID) => {
        API.delete(`movies/${movieID}`).then((res) => {
            getMovieList();
        });
    };

    const BtnDelete = ({ callbackMovieID }) => {
        if (roleid === '0') {
            return (
                [
                    <Button variant="danger" onClick={() => deleteMovie(callbackMovieID)}>Hapus</Button>
                ]
            )
        } else {
            return null;
        }
    }

    const handleClose = () => setShowEditForm(false);

    const handleShowFormEdit = (movieID, movieTitle, movieSynopsis, movieGenre, moviePoster, movieStatus) => {
        setShowEditForm(true);
        setEditMovieID(movieID);
        setEditTitle(movieTitle);
        setEditSynopsis(movieSynopsis);
        setEditGenre(movieGenre);
        setEditPoster(moviePoster);
        setEditStatus(movieStatus);

        if (roleid === '0') {
            setInputReadonly(false);
        } else {
            setInputReadonly(true);
        }
    };

    const editDataMovie = (event) => {
        event.preventDefault();
        event.stopPropagation();
        let movietitle, moviesynopsis, movieposter, moviegenre;

        if (roleid !== '0') {
            movietitle = editTitle;
            moviesynopsis = editSynopsis;
            movieposter = editPoster;
            moviegenre = editGenre;
        } else {
            movietitle = event.target.addTitle.value;
            moviesynopsis = event.target.addSynopsis.value;
            movieposter = event.target.addPoster.value;
            moviegenre = event.target.listGenre.value;
        }

        API.put(`movies/${editMovieID}`, {
            title: movietitle,
            synopsis: moviesynopsis,
            poster: movieposter,
            genre: moviegenre,
            status: event.target.listStatus.value

        }).then(res => {
            const { status } = res;
            if (status === 200 | status === 201) {
                handleClose();
                getMovieList();
            }

            console.log(res);
            console.log(res.data);
        });
    }

    return (
        <Container>
            <Table responsive>
                <thead>
                    <tr>
                        <th>Judul Film</th>
                        <th>Sinopsis</th>
                        <th>Genre</th>
                        <th>Poster</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {moviedata.length ? (
                        moviedata.map((dataAPI) => (
                            <tr key={dataAPI._id}>
                                <td>{dataAPI.title}</td>
                                <td>{dataAPI.synopsis}</td>
                                <td>{dataAPI.genre}</td>
                                <td>
                                    <img
                                        className="poster-list"
                                        src={dataAPI.poster}
                                        alt={dataAPI.title}
                                    />
                                </td>
                                <td>{dataAPI.status}</td>
                                <td>
                                    <Button
                                        variant="info"
                                        onClick={() =>
                                            handleShowFormEdit(dataAPI._id, dataAPI.title, dataAPI.synopsis, dataAPI.genre, dataAPI.poster, dataAPI.status)
                                        }
                                    >
                                        Ubah
                                    </Button>
                                    <BtnDelete callbackMovieID={dataAPI._id} />
                                </td>
                            </tr>
                        ))
                    ) : (
                            <tr>
                                <td colspan="6">
                                    <Spinner animation="grow" variant="primary" />
                                    <Spinner animation="grow" variant="secondary" />
                                    <Spinner animation="grow" variant="success" />
                                    <Spinner animation="grow" variant="danger" />
                                    <Spinner animation="grow" variant="warning" />
                                    <Spinner animation="grow" variant="info" />
                                    <Spinner animation="grow" variant="light" />
                                    <Spinner animation="grow" variant="dark" />
                                </td>
                            </tr>
                        )}
                </tbody>
            </Table>

            <Modal show={showEditForm} onHide={handleClose}>
                <Form onSubmit={editDataMovie}>
                    <Modal.Header closeButton>
                        <Modal.Title>Form Perubahan Data Movie</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <Form.Group as={Row} controlId="formHorizontalTitle">
                            <Form.Label column sm={2}>
                                Judul Film
                                </Form.Label>
                            <Col sm={10}>
                                {isInputReadonly ?
                                    <div><p>{editTitle}</p></div>
                                    :
                                    <Form.Control
                                        type="title"
                                        placeholder="Judul Film"
                                        required
                                        name="addTitle"
                                        defaultValue={editTitle}
                                        readonly={isInputReadonly}
                                    />
                                }
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row} controlId="formHorizontalSynopsis">
                            <Form.Label column sm={2}>
                                Sinopsis
                                </Form.Label>
                            <Col sm={10}>
                                {isInputReadonly ?
                                    <div><p>{editSynopsis}</p></div>
                                    :
                                    <Form.Control
                                        type="synopsis"
                                        placeholder="Sinopsis"
                                        required
                                        name="addSynopsis"
                                        defaultValue={editSynopsis}
                                    />
                                }
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row} controlId="formHorizontalPoster">
                            <Form.Label column sm={2}>
                                Poster
                                </Form.Label>
                            <Col sm={10}>
                                {isInputReadonly ?
                                    <div><img class="editform-poster" src={editPoster} alt={editTitle} /></div>
                                    :
                                    <Form.Control
                                        type="poster"
                                        placeholder="Poster"
                                        required
                                        name="addPoster"
                                        defaultValue={editPoster}
                                    />
                                }
                            </Col>
                        </Form.Group>

                        <fieldset>
                            <Form.Group as={Row}>
                                <Form.Label as="legend" column sm={2}>
                                    Genre
                                    </Form.Label>
                                <Col sm={10}>
                                    {isInputReadonly ?
                                        <div><p>{editGenre}</p></div>
                                        :
                                        <div>
                                            <Form.Check
                                                type="radio"
                                                label="Komedi"
                                                name="listGenre"
                                                id="formHorizontalRadios1"
                                                value="comedy"
                                                defaultChecked={editGenre === 'comedy'}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Romantis"
                                                name="listGenre"
                                                id="formHorizontalRadios2"
                                                value="romance"
                                                defaultChecked={editGenre === 'romance'}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Kartun"
                                                name="listGenre"
                                                id="formHorizontalRadios3"
                                                value="cartoon"
                                                defaultChecked={editGenre === 'cartoon'}
                                            />
                                        </div>
                                    }
                                </Col>
                            </Form.Group>
                        </fieldset>

                        <Form.Group controlId="formHorizontalStatus" as={Row}>
                            <Form.Label column sm={2}>Status</Form.Label>
                            <Col sm={10}>
                                <Form.Control name="listStatus" as="select" defaultValue={editStatus || defaultMovieStatus}>
                                    <option value="pending">Pending</option>
                                    <option value="pre-release">Pre-Release/On Review</option>
                                    <option value="released">Released/On Cinema/On Sales</option>
                                    <option value="taken-down">Taken Down</option>
                                </Form.Control>
                            </Col>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Batalkan Perubahan
                        </Button>
                        <Button type submit variant="primary">
                            Simpan Perubahan
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Container>
    );
};

export default Dashboard;
