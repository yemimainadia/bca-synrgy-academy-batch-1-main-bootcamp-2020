import React, { useState, useEffect } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { useHistory } from "react-router-dom";

const Sidebar = () => {
    const [roleid, setRoleid] = useState("");

    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem('token-roleID')) {
            setRoleid(localStorage.getItem('token-roleID'))
        }
    });
    const addMovie = () => {
        history.push("/add-movie");
    }

    const Navigation = () => {
        if (roleid === '0') {
            return (
                [
                    <NavDropdown title="Admin" id="nav-dropdown">
                        <NavDropdown.Item eventKey="4.1" onSelect={addMovie}>Tambah Film</NavDropdown.Item>
                        {/*<NavDropdown.Item eventKey="4.2">Ubah Film</NavDropdown.Item>
                        <NavDropdown.Item eventKey="4.3">Hapus Film</NavDropdown.Item>*/}
                    </NavDropdown>
                ]
            )
        } else {
            return null;
        }
    }

    const Logout = () => {
        localStorage.removeItem('token-roleID');
        localStorage.removeItem('token-customerID');
        history.push("/");
    }

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Bioskop21</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Navigation />
                    <NavDropdown title="Profil" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Profil Saya</NavDropdown.Item>
                        <NavDropdown.Item onClick={Logout}>Keluar</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default Sidebar;
