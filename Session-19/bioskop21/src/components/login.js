import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import API from '../api';
import { useHistory } from "react-router-dom";


const Login = () => {
    const [validated, setValidated] = useState(false);

    const history = useHistory();
    useEffect(() => {
        if (localStorage.getItem('token-customerID')) {
            alert('dah login' + localStorage.getItem('token-customerID'));
            history.push("/menu");
        }
    });
    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        event.stopPropagation();



        if (form.checkValidity() !== false) {
            const dataUser = {
                email: event.target.loginEmail.value,
                password: event.target.loginPassword.value
            }
            console.log(dataUser);
            const queryGetUser = '/users?q=' + JSON.stringify(dataUser);

            API.get(queryGetUser)
                .then(response => {
                    const { data } = response;
                    console.log(data);
                    const userData = data[0];
                    if (data.length === 1 && userData.active === true) {
                        localStorage.setItem('token-customerID', userData._id);
                        localStorage.setItem('token-roleID', userData.role);
                        history.push("/menu");
                    }
                })
                .catch(error => {
                    console.log(error)
                });

        }
    }

    return (
        <Container>
            <div className="wrapper"> <img className="logo" src="https://upload.wikimedia.org/wikipedia/id/c/cd/21_Cineplex_logo.png" alt="21" /> </div>
            <Form noValidate validated={validated} onSubmit={handleSubmit} className="form-login">
                <Form.Group controlId="loginEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required name="loginEmail" className="input-email" />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
          </Form.Text>
                </Form.Group>

                <Form.Group controlId="loginPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required name="loginPassword" className="input-password" />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </Container>
    );
}

export default Login;


