import React from 'react';
import Login from './login';
import Layout from '../dashboard/layout';
import FormAdd from '../dashboard/formAdd';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

const Page = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Login />
            </Route>
            <Route path="/menu">
                <Layout />
            </Route>
            <Route path="/add-movie">
                <FormAdd />
            </Route>
        </Switch>
    );
}

export default Page;
