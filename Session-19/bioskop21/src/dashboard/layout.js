import React from 'react';
import Sidebar from '../components/sidebar';
import Table from '../components/table';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const layout = () => {
    return (
        <Row>
            <Col xs={12} md={12}>
                <Sidebar />
                <Table />
            </Col>
        </Row>
    );
}

export default layout;
