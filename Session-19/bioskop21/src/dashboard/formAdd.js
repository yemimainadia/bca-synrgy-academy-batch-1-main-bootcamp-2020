import React, { useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/esm/Container';
import API from '../api';
import { useHistory } from "react-router-dom";

const FormAdd = () => {

    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem('token-roleID') !== '0') {
            history.push("/menu");
        }
    });

    const addFilm = (event) => {
        event.preventDefault();
        event.stopPropagation();

        API.post('movies', {
            title: event.target.addTitle.value,
            synopsis: event.target.addSynopsis.value,
            poster: event.target.addPoster.value,
            genre: event.target.listGenre.value,
            status: 'pending'

        }).then(res => {
            const { status } = res;
            if (status === 201 || status === 200) {
                history.push("/menu");
                alert('Data berhasil ditambahkan')
            }
            console.log(res);
            console.log(res.data);
        });
    }

    return (
        <Container>
            <Form onSubmit={addFilm}>
                <Form.Group as={Row} controlId="formHorizontalTitle">
                    <Form.Label column sm={2}>
                        Judul Film
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="title" placeholder="Judul Film" required name="addTitle" />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalSynopsis">
                    <Form.Label column sm={2}>
                        Sinopsis
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="synopsis" placeholder="Sinopsis" required name="addSynopsis" />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalPoster">
                    <Form.Label column sm={2}>
                        Poster
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="poster" placeholder="Poster" required name="addPoster" />
                    </Col>
                </Form.Group>

                <fieldset>
                    <Form.Group as={Row}>
                        <Form.Label as="legend" column sm={2}>
                            Genre
      </Form.Label>
                        <Col sm={10}>
                            <Form.Check
                                type="radio"
                                label="Komedi"
                                name="listGenre"
                                id="formHorizontalRadios1"
                                value="comedy"
                            />
                            <Form.Check
                                type="radio"
                                label="Romantis"
                                name="listGenre"
                                id="formHorizontalRadios2"
                                value="romance"
                            />
                            <Form.Check
                                type="radio"
                                label="Kartun"
                                name="listGenre"
                                id="formHorizontalRadios3"
                                value="cartoon"
                            />
                        </Col>
                    </Form.Group>
                </fieldset>

                <Form.Group as={Row}>
                    <Col sm={{ span: 10, offset: 2 }}>
                        <Button type="submit">Tambah</Button>
                    </Col>
                </Form.Group>

            </Form>
        </Container>
    );
}

export default FormAdd;
