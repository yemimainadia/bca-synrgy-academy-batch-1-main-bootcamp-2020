import React from 'react';
import '../src/index.css';

import {
  BrowserRouter as Router
} from "react-router-dom";
import Page from './components/page';

function App() {
  return (
    <Router>
      <div className="App">
        <Page />
      </div>
    </Router>
  );
}

export default App;
