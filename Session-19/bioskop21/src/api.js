import axios from 'axios';
const keyApi = 'bdcaa57c2d76dc7c37a54b2203fb84e10eb97';

export default axios.create({
    baseURL: 'https://rolebase-98ec.restdb.io/rest/',
    timeout: 15000,
    "async": true,
    "crossDomain": true,
    'Access-Control-Allow-Origin': true,
    "headers": {
        "content-type": "application/json",
        "x-apikey": keyApi,
        "cache-control": "no-cache"
    }
});